# Isaiah M. Bernal
# Shayan A. Far

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase
from Diplomacy import diplomacy_solve

# -------------
# TestDiplomacy
# -------------


class TestDiplomacy (TestCase):

    # Test Cases given on project spec site

    def test_diplomacy_given_1(self):
        r = StringIO("A Madrid Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid")

    def test_diplomacy_given_2(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London")

    def test_diplomacy_given_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]")

    def test_diplomacy_given_4(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]")

    def test_diplomacy_given_5(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]")

    def test_diplomacy_given_6(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris")

    def test_diplomacy_given_7(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin")

    # Test cases we made up

    def test_diplomacy_homebrew_1(self):
        r = StringIO("A Houston Support E\nB Dallas Support C\nC SanAntonio Hold\nD Trenton Move Glasgow\nE Glasgow Move SanAntonio\nF FortWorth Support E\nG Tokyo Support H\nH Osaka Move Tokyo")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(
        ), "A Houston\nB Dallas\nC [dead]\nD Glasgow\nE SanAntonio\nF FortWorth\nG [dead]\nH [dead]")

    def test_diplomacy_homebrew_2(self):
        r = StringIO("A Dharka Support D\nB Shanghai Move Mumbai\nC Cairo Support D\nD Mumbai Move Beijing\nE Beijing Hold\nF Arlington Move Cairo\nG MexicoCity Move Shanghai\nH Moscow Move Dharka")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(
        ), "A [dead]\nB Mumbai\nC [dead]\nD [dead]\nE [dead]\nF [dead]\nG Shanghai\nH [dead]")

    def test_diplomacy_homebrew_3(self):
        r = StringIO("A Chongqing Support D\nB Lagos Move Singapore\nC Istanbul Support D\nD SaoPaulo Move Tehran\nE Tehran Support B\nF Surat Support B\nG Singapore Support F")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(
        ), "A Chongqing\nB Singapore\nC Istanbul\nD Tehran\nE [dead]\nF Surat\nG [dead]")

    def test_diplomacy_homebrew_4(self):
        r = StringIO(
            "A Madrid Move Vegas\nB Houston Move Vegas\nC Spain Move Vegas\nD Austin Move Vegas\nE Paris Hold\nF Vegas Move Paris")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\nE [dead]\nF [dead]")

    def test_diplomacy_homebrew_5(self):
        r = StringIO(
            "A Madrid Support B\nB Houston Support C\nC Spain Support D\nD Austin Support E\nE Paris Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB Houston\nC Spain\nD Austin\nE Paris")

    def test_diplomacy_homebrew_6(self):
        r = StringIO(
            "A Madrid Move Houston\nB Houston Support C\nC Spain Support D\nD Austin Support E\nE Paris Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Houston\nB [dead]\nC Spain\nD Austin\nE Paris")

    def test_diplomacy_homebrew_7(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Barcelona")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC Barcelona")

    def test_diplomacy_homebrew_8(self):
        r = StringIO("A Texas Move Florida\nB Florida Move Utah\nC Utah Move Delaware\nD Delaware Move NorthDakota\nE NorthDakota Move RhodeIsland\nF RhodeIsland Move Lousiana\nG Lousiana Move Kansas\nH Kansas Move Texas")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(
        ), "A Florida\nB Utah\nC Delaware\nD NorthDakota\nE RhodeIsland\nF Lousiana\nG Kansas\nH Texas")

# ----
# main
# ----


if __name__ == "__main__":  # pragma: no cover
    main()
