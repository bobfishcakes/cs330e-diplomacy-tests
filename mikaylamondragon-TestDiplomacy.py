#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):

    def test_solve(self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Support B')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(),'A [dead]\nB Madrid\nC London')

    def test_solve_2(self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Support B')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(),'A [dead]\nB Madrid\nC London')

    def test_solve_3(self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(),'A [dead]\nB Madrid\nC [dead]\nD Paris')

    def test_solve_4(self):
        r = StringIO('')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(),'')

# ----
# main
# ----


if __name__ == "__main__":
    main()